version: '2'

services:
  zammad-backup:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    command: ["zammad-backup"]
    depends_on:
      - zammad-railsserver
    entrypoint: /usr/local/bin/backup.sh
    image: zammad/zammad-docker-compose:zammad-postgresql-2.6.0
    links:
      - zammad-postgresql
    restart: always
    volumes:
      - zammad-backup:/var/tmp/zammad
      - zammad-data:/opt/zammad

  zammad-elasticsearch:
    image: zammad/zammad-docker-compose:zammad-elasticsearch-2.6.0
    labels:
      zammad: true
      {{- if eq .Values.UPDATE_SYSCTL "true" }}
      io.rancher.sidekicks: zammad-elasticsearch-sysctl
      {{- end}}
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    restart: always
    volumes:
      - elasticsearch-data:/usr/share/elasticsearch/data

  {{- if eq .Values.UPDATE_SYSCTL "true" }}
  zammad-elasticsearch-sysctl:
    labels:
      zammad: true
      io.rancher.container.start_once: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    network_mode: none
    image: rawmind/alpine-sysctl:0.1
    privileged: true
    environment:
        - "SYSCTL_KEY=vm.max_map_count"
        - "SYSCTL_VALUE=262144"
  {{- end}}

  zammad-init:
    command: ["zammad-init"]
    depends_on:
      - zammad-postgresql
    image: zammad/zammad-docker-compose:zammad-2.6.0
    labels:
      zammad: true
      io.rancher.container.start_once: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    links:
      - zammad-elasticsearch
      - zammad-postgresql
    restart: on-failure
    volumes:
      - zammad-data:/opt/zammad

  zammad-memcached:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    command: ["zammad-memcached"]
    image: zammad/zammad-docker-compose:zammad-memcached-2.6.0
    restart: always

  zammad-nginx:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    command: ["zammad-nginx"]
    depends_on:
      - zammad-railsserver
    image: zammad/zammad-docker-compose:zammad-2.6.0
    links:
      - zammad-railsserver
      - zammad-websocket
    restart: always
    volumes:
      - zammad-data:/opt/zammad

  zammad-postgresql:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    image: zammad/zammad-docker-compose:zammad-postgresql-2.6.0
    restart: always
    volumes:
      - postgresql-data:/var/lib/postgresql/data

  zammad-railsserver:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    command: ["zammad-railsserver"]
    depends_on:
      - zammad-memcached
      - zammad-postgresql
    image: zammad/zammad-docker-compose:zammad-2.6.0
    links:
      - zammad-elasticsearch
      - zammad-memcached
      - zammad-postgresql
    restart: always
    volumes:
      - zammad-data:/opt/zammad

  zammad-scheduler:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    command: ["zammad-scheduler"]
    depends_on:
      - zammad-memcached
      - zammad-railsserver
    image: zammad/zammad-docker-compose:zammad-2.6.0
    links:
      - zammad-elasticsearch
      - zammad-memcached
      - zammad-postgresql
    restart: always
    volumes:
      - zammad-data:/opt/zammad

  zammad-websocket:
    labels:
      zammad: true
      {{- if ne .Values.HOST_LABEL }}
      io.rancher.scheduler.affinity:host_label: ${HOST_LABEL}
      {{- end}}
    command: ["zammad-websocket"]
    depends_on:
      - zammad-memcached
      - zammad-railsserver
    image: zammad/zammad-docker-compose:zammad-2.6.0
    links:
      - zammad-postgresql
      - zammad-memcached
    restart: always
    volumes:
      - zammad-data:/opt/zammad

volumes:
  elasticsearch-data:
    driver: local
  postgresql-data:
    driver: ${VOLUME_DRIVER}
  zammad-backup:
    driver: ${VOLUME_DRIVER}
  zammad-data:
    driver: ${VOLUME_DRIVER}
