# Bezerras Rancher templates

Since community rancher templates seens to be not maintained anymore, I created
this repository to have more updated tools in your rancher.

## Install

In Rancher's UI, go to **Admin/Settings** and add a new custom catalog:

| Name              | URL                                                 | Branch |
| ----------------- | --------------------------------------------------- | ------ |
| Bezerras template | https://gitlab.com/paulobezerra/rancher-catalog.git | master |

